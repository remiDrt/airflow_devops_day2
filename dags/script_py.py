import datetime
import requests
import pymongo
import os

from datetime import timedelta

import airflow
from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

#!/usr/bin/env python

try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen

import certifi
import json

def get_jsonparsed_data(url):
    """
    Receive the content of ``url``, parse it as JSON and return the object.

    Parameters
    ----------
    url : str

    Returns
    -------
    dict
    """
    response = urlopen(url, cafile=certifi.where())
    data = response.read().decode("utf-8")
    return json.loads(data)


args = {
    'id' : 'project_dag',
    'owner':'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),

}


dag = DAG(
    'apple_dag',
    default_args=args,
    schedule_interval=timedelta(minutes=1),
)


alsorunthis = BashOperator(
        task_id = 'alsorunthis',
        bash_command = 'echo "hello"',
        dag = dag

    )

def dostuff():
    """
    Insert data from apple in MongoDB
    """
    stocks_price_url = ("https://financialmodelingprep.com/api/v3/quote-short/AAPL?apikey=1cae64dd039a417181f92d21e747aebe")
    profile_url = ("https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=1cae64dd039a417181f92d21e747aebe")
    rating_url = ("https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=1cae64dd039a417181f92d21e747aebe")

    stock_price = get_jsonparsed_data(stocks_price_url)
    profile = get_jsonparsed_data(profile_url)
    rating = get_jsonparsed_data(rating_url)

    data = {
        'profile': profile,
        'rating': rating,
        'timestamp': datetime.datetime.now().timestamp()
    }

    #print(data)
    MONGO_DB = os.environ.get('MONGO_INITDB_DATABASE')
    client = pymongo.MongoClient("mongodb://example:example@mongo:27017")
    print(client)
    client.list_databases()
    db = client.apple_stocks
    print(db)
    collection = db.appleCollection
    print(collection)

    data_ins = collection.insert_one(data)
    print("Data inserted with record ids", data_ins)

    # Printing the data inserted
    cursor = collection.find()
    for record in cursor:
        print(record)

    print("Hello les Fipas")
    #raise Exception('spam', 'eggs')

pythonop = PythonOperator(
        task_id = 'pythonrun',
        python_callable = dostuff,
        dag = dag
    )

alsorunthis >> pythonop