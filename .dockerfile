FROM puckel/docker-airflow:1.10.9
ENV PYTHONPATH "${PYTHONPATH}:/usr/local/airflow"
USER root
RUN apt-get update
RUN pip3 install pymongo
